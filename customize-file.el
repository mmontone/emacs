;; Use Emacs customization mode to configure your applications, in any language.
;; Setting are saved in JSON format.

(require 'json)

;;(symbol-plist 'esb:asdf-system)
;;(get 'esb:asdf-system 'custom-type)
;;(get 'esb:asdf-system 'saved-value)

;; (custom-buffer-create-other-window
;;  (list (list 'system-browser 'custom-group))
;;  "Customize System Browser"
;;  "This is a buffer for customizing System Browser")

;; (custom-buffer-create
;;  (list (list 'system-browser 'custom-group))
;;  "Customize System Browser"
;;  "This is a buffer for customizing System Browser")

;; (symbol-plist 'system-browser)
;; (get 'system-browser 'custom-group)

;; (get 'emacs 'custom-group)

;; USE THE NAME OF THE CUSTOMIZATION FILE TO IDENTIFY THE NAME OF THE MAIN GROUP.
;; For example, for configuring project named myapp, use myapp.custom file name.

(defun customize-file-set-variable (var value)
  ;;(put var 'saved-value (list value))
  (set var value))

;;(json-read-file "~/src/bower.json")
;;(json-encode-alist '((value . "foo")))
;;(json-encode-alist '((value . ("foo" "bar" "baz"))))

;;(custom-group-members 'bonanza nil)
;;(custom-group-members 'bonanza-general nil)

(defun all-custom-group-variables (group-name)
  "Get all the custom-variables that belong to custom-group named GROUP-NAME,including its subgroups."
  (let ((variables '()))
    (dolist (member (custom-group-members group-name nil))
      (case (second member)
	(custom-group
	 (setq variables (nconc variables (all-custom-group-variables (first member)))))
	(custom-variable (push (first member) variables))))
    variables))

;; (all-custom-group-variables 'bonanza)
;; (symbol-value 'bonanza-root-dir)

(defun custom-group-values-alist (custom-group)
  "Get the list of values of variables belonging to CUSTOM-GROUP, in an alist."
  (let ((var-values))
    (dolist (var (all-custom-group-variables custom-group))
      (push (cons var (symbol-value var)) var-values))
    var-values))

(defun set-custom-variables-values (alist)
  "Set the values of custom variables in ALIST."
  (dolist (var-value alist)
    (set (car var-value) (cdr var-value))))

;; (custom-group-values-alist 'bonanza)

;; (json-encode-alist (custom-group-values-alist 'bonanza))
;; (json-read-from-string (json-encode-alist (custom-group-values-alist 'bonanza)))

(defun save-custom-group-values-to-file (group-name file)
  (with-temp-file file
    (insert (json-encode-alist (custom-group-values-alist group-name)))))

;;(save-custom-group-values-to-file 'bonanza "/home/marian/work/bonanza/config/bonanza.default")

(defun load-custom-variables-values-from-file (file)
  (set-custom-variables-values (json-read-file file)))

;;(load-custom-variables-values-from-file "/home/marian/work/bonanza/config/bonanza.default")

(defun customize-file (file)
  "Load customization file FILE and edit it in Emacs customize mode."
  (interactive "fCustomize file: ")
  
  ;; Find customization file and load it
  (let ((customization-file (format "%s/%s.custom"
				    (file-name-directory file)
				    (file-name-base file))))
    (load-file customization-file)
    (load-custom-variables-values-from-file file)
    (customize-group (file-name-base file))))

;;(customize-file "/home/marian/work/bonanza/config/bonanza.default")

(defun customize-file--customize-group (group &optional other-window)
  "Customize GROUP, which must be a customization group.
If OTHER-WINDOW is non-nil, display in another window."

  (when (stringp group)
    (if (string-equal "" group)
	(setq group 'emacs)
      (setq group (intern group))))
  (let ((name (format "*Customize Group: %s*"
		      (custom-unlispify-tag-name group))))
    (cond
     ((null (get-buffer name))
      (funcall (if other-window
		   'customize-file--custom-buffer-create-other-window
		 'customize-file--custom-buffer-create)
	       (list (list group 'custom-group))
	       name
	       (concat " for group "
		       (custom-unlispify-tag-name group))))
     (other-window
      (switch-to-buffer-other-window name))
     (t
      (pop-to-buffer-same-window name)))))

(defun customize-file--custom-buffer-create-other-window (options &optional name _description)
  "Create a buffer containing OPTIONS, and display it in another window.
The result includes selecting that window.
Optional NAME is the name of the buffer.
OPTIONS should be an alist of the form ((SYMBOL WIDGET)...), where
SYMBOL is a customization option, and WIDGET is a widget for editing
that option.
DESCRIPTION is unused."
  (unless name (setq name "*Customization*"))
  (switch-to-buffer-other-window (custom-get-fresh-buffer name))
  (customize-file--custom-buffer-create-internal options))

(defun customize-file--custom-buffer-create-internal (options &optional _description)
  (Custom-mode)
  (setq custom--invocation-options options)
  (let ((init-file (or custom-file user-init-file)))
    ;; Insert verbose help at the top of the custom buffer.
    (when custom-buffer-verbose-help
      (unless init-file
	(widget-insert
         (format-message
          "Custom settings cannot be saved; maybe you started Emacs with `-q'.\n")))
      (widget-insert "For help using this buffer, see ")
      (widget-create 'custom-manual
		     :tag "Easy Customization"
		     "(emacs)Easy Customization")
      (widget-insert " in the ")
      (widget-create 'custom-manual
		     :tag "Emacs manual"
		     :help-echo "Read the Emacs manual."
		     "(emacs)Top")
      (widget-insert "."))
    (widget-insert "\n")

    ;; Insert the search field.
    (when custom-search-field
      (widget-insert "\n")
      (let* ((echo "Search for custom items.
You can enter one or more words separated by spaces,
or a regular expression.")
	     (search-widget
	      (widget-create
	       'editable-field
	       :size 40 :help-echo echo
	       :action (lambda (widget &optional _event)
                         (customize-apropos (split-string (widget-value widget)))))))
	(widget-insert " ")
	(widget-create-child-and-convert
	 search-widget 'push-button
	 :tag " Search "
	 :help-echo echo :action
	 (lambda (widget &optional _event)
	   (customize-apropos (split-string (widget-value (widget-get widget :parent))))))
	(widget-insert "\n")))

    ;; The custom command buttons are also in the toolbar, so for a
    ;; time they were not inserted in the buffer if the toolbar was in use.
    ;; But it can be a little confusing for the buffer layout to
    ;; change according to whether or nor the toolbar is on, not to
    ;; mention that a custom buffer can in theory be created in a
    ;; frame with a toolbar, then later viewed in one without.
    ;; So now the buttons are always inserted in the buffer.  (Bug#1326)
    (if custom-buffer-verbose-help
	(widget-insert "
Operate on all settings in this buffer:\n"))
    (let ((button (lambda (tag action visible help _icon _label active)
		    (widget-insert " ")
                    (if (eval visible)
                        (push (widget-create
                               'push-button :tag tag
                               :help-echo help :action action
                               :notify
                               (lambda (widget)
                                 (when (listp active)
                                   (if (seq-some
                                        (lambda (widget)
                                          (memq
                                           (widget-get widget :custom-state)
                                           active))
                                        custom-options)
                                       (widget-apply widget :activate)
                                     (widget-apply widget :deactivate)))))
                              custom-command-buttons))))
	  (commands custom-commands))
      (if custom-reset-button-menu
	  (progn
	    (widget-create 'push-button
			   :tag " Revert... "
			   :help-echo "Show a menu with reset operations."
			   :mouse-down-action 'ignore
			   :action 'custom-reset)
	    (apply button (pop commands))  ; Apply
	    (apply button (pop commands))) ; Apply and Save
	(apply button (pop commands))   ; Apply
	(apply button (pop commands))   ; Apply and Save
	(widget-insert "\n")
	(apply button (pop commands))   ; Undo
	(apply button (pop commands))   ; Reset
	(apply button (pop commands))   ; Erase
	(widget-insert "  ")
	(pop commands)                  ; Help (omitted)
	(apply button (pop commands)))) ; Exit
    (widget-insert "\n\n"))

  ;; Now populate the custom buffer.
  (message "Creating customization items...")
  (buffer-disable-undo)
  (setq custom-options
	(if (= (length options) 1)
	    (mapcar (lambda (entry)
		      (widget-create (nth 1 entry)
				     :documentation-shown t
				     :custom-state 'unknown
				     :tag (custom-unlispify-tag-name
					   (nth 0 entry))
				     :value (nth 0 entry)))
		    options)
	  (let ((count 0)
		(length (length options)))
	    (mapcar (lambda (entry)
		      (prog2
			  (message "Creating customization items ...%2d%%"
				   (floor (* 100.0 count) length))
			  (widget-create (nth 1 entry)
					 :tag (custom-unlispify-tag-name
					       (nth 0 entry))
					 :value (nth 0 entry))
			(setq count (1+ count))
			(unless (eq (preceding-char) ?\n)
			  (widget-insert "\n"))
			(widget-insert "\n")))
		    options))))
  (unless (eq (preceding-char) ?\n)
    (widget-insert "\n"))
  (message "Creating customization items ...done")
  (unless (eq custom-buffer-style 'tree)
    (mapc 'custom-magic-reset custom-options))
  (message "Creating customization setup...")
  (widget-setup)
  (buffer-enable-undo)
  (goto-char (point-min))
  (message "Creating customization setup...done"))

(defun customize-file--custom-buffer-create (options &optional name _description)
  "Create a buffer containing OPTIONS.
Optional NAME is the name of the buffer.
OPTIONS should be an alist of the form ((SYMBOL WIDGET)...), where
SYMBOL is a customization option, and WIDGET is a widget for editing
that option.
DESCRIPTION is unused."
  (pop-to-buffer-same-window
   (custom-get-fresh-buffer (or name "*Customization*")))
  (customize-file--custom-buffer-create-internal options)
  ;; Notify the command buttons, to correctly enable/disable them.
  (dolist (btn custom-command-buttons)
    (widget-apply btn :notify)))



