(push "~/.emacs.d/lisp" load-path)

(defconst user-init-dir
  (cond ((boundp 'user-emacs-directory)
         user-emacs-directory)
        ((boundp 'user-init-directory)
         user-init-directory)
        (t "~/.emacs.d/"))
  "User init directory")

(defun load-user-file (file)
  (interactive "f")
  "Load a file in current user's configuration directory"
  (load-file (expand-file-name file user-init-dir)))

(when (>= emacs-major-version 24)
  (require 'package)
  (add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
  (package-initialize)
  ;;(add-to-list 'package-archives '("marmalade" . "http://marmalade-repo.org/packages/"))
  )


(projectile-mode +1)
(define-key projectile-mode-map (kbd "s-p") 'projectile-command-map)
(define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)

(defun indent-buffer ()
  "indent whole buffer"
  (interactive)
  (delete-trailing-whitespace)
  (indent-region (point-min) (point-max) nil)
  (untabify (point-min) (point-max)))

(load-user-file "slime.el")

(defun slime-info-apropos (symbol-name)
  (interactive (list (slime-read-symbol-name "Apropos symbol info: ")))
  (when (not symbol-name)
    (error "No symbol given"))
  (if (position 58 symbol-name) ;; 58 is the colon character
      (info-apropos symbol-name)
    (let* ((symbol-package-name
            (slime-eval
             `(cl:package-name
               (cl:symbol-package (cl:read-from-string ,(concat (remove 58 (slime-current-package)) "::" symbol-name))))))
           (index-entry (concat symbol-package-name ":" symbol-name)))
      (info-apropos index-entry))))

(add-hook 'lisp-mode-hook
          (lambda () (local-set-key (kbd "C-c h") 'slime-info-apropos)))

(add-hook 'slime-repl-mode-hook
          (lambda () (local-set-key (kbd "C-c h") 'slime-info-apropos)))

(add-hook 'lisp-mode-hook 'highlight-symbol-mode)

(add-hook 'Info-selection-hook 'info-colors-fontify-node)

(require 'helpful)

;; Note that the built-in `describe-function' includes both functions
;; and macros. `helpful-function' is functions only, so we provide
;; `helpful-callable' as a drop-in replacement.
(global-set-key (kbd "C-h f") #'helpful-callable)

(global-set-key (kbd "C-h v") #'helpful-variable)
(global-set-key (kbd "C-h k") #'helpful-key)

;; Lookup the current symbol at point. C-c C-d is a common keybinding
;; for this in lisp modes.
(global-set-key (kbd "C-c C-d") #'helpful-at-point)

;; Look up *F*unctions (excludes macros).
;;
;; By default, C-h F is bound to `Info-goto-emacs-command-node'. Helpful
;; already links to the manual, if a function is referenced there.
(global-set-key (kbd "C-h F") #'helpful-function)

;; Look up *C*ommands.
;;
;; By default, C-h C is bound to describe `describe-coding-system'. I
;; don't find this very useful, but it's frequently useful to only
;; look at interactive functions.
(global-set-key (kbd "C-h C") #'helpful-command)

;;(load-theme 'kaolin t)
;;(load-theme 'leuven t)
;;(load-theme 'material-light t)
;;(load-theme 'mccarthy t)
;;(load-theme 'tango-plus t)
;;(load-theme 'moe-light t)
(load-theme 'kaolin-light t)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-names-vector
   ["#383e3f" "#e84c58" "#13665F" "#E36B3F" "#3B84CC" "#a9779c" "#6facb3" "#C8CCC3"])
 '(centaur-tabs-set-icons t)
 '(centaur-tabs-show-navigation-buttons t)
 '(centaur-tabs-style "chamfer")
 '(custom-raised-buttons t)
 '(custom-safe-themes
   '("3c7a784b90f7abebb213869a21e84da462c26a1fda7e5bd0ffebf6ba12dbd041" "d516f1e3e5504c26b1123caa311476dc66d26d379539d12f9f4ed51f10629df3" "11cc65061e0a5410d6489af42f1d0f0478dbd181a9660f81a692ddc5f948bf34" "2ce76d65a813fae8cfee5c207f46f2a256bac69dacbb096051a7a8651aa252b0" "cf9414f229f6df728eb2a5a9420d760673cca404fee9910551caf9c91cff3bfa" "d9a28a009cda74d1d53b1fbd050f31af7a1a105aa2d53738e9aa2515908cac4c" "6f895d86fb25fac5dd4fcce3aec0fe1d88cf3b3677db18a9607cf7a3ef474f02" "c433c87bd4b64b8ba9890e8ed64597ea0f8eb0396f4c9a9e01bd20a04d15d358" "0feb7052df6cfc1733c1087d3876c26c66410e5f1337b039be44cb406b6187c6" "16ce45f31cdea5e74ca4d27519d7ebe998d69ec3bf7df7be63c5ffdb5638b387" "c3e6b52caa77cb09c049d3c973798bc64b5c43cc437d449eacf35b3e776bf85c" "3cd28471e80be3bd2657ca3f03fbb2884ab669662271794360866ab60b6cb6e6" "89885317e7136d4e86fb842605d47d8329320f0326b62efa236e63ed4be23c58" "304aa6c01aea430cb774c82c6151823ac4dd5b7a3ceb3b5c5c12b304931dd09d" "fc6697788f00629cd01f4d2cc23f1994d08edb3535e4c0facef6b7247b41f5c7" default))
 '(custom-search-field t)
 '(debug-on-error t)
 '(eldoc-overlay-backend 'inline-docs)
 '(electric-quote-mode nil)
 '(esb:asdf-system '(""))
 '(highlight-symbol-idle-delay 0.5)
 '(package-selected-packages
   '(rainbow-mode desktop-environment dired-icon exwm restclient pass snow doom-modeline crux which-key json-navigator eldoc-overlay centaur-tabs e2t prism gopher popwin direx-grep direx dir-treeview shampoo rainbow-delimiters selectrum-prescient vertico tron-legacy-theme dashboard parsec elfeed kaolin-themes anaphora solarized-theme moe-theme tango-plus-theme apropospriate-theme sublime-themes material-theme tangotango-theme zerodark-theme zenburn-theme yaml-mode xref-js2 wgrep weechat web-mode w3m vue-mode utop use-package treemacs-projectile tree-mode template-overlays smartparens slime-company slime-annot selectrum select-themes rnc-mode regex-dsl redshank python-docstring psgml project-explorer pretty-symbols pretty-mode-plus pretty-mode pretty-lambdada prettify-greek pony-mode poly-org plantuml-mode peg nord-theme niceify-info navi-mode mustache-mode multifiles multi-web-mode monokai-theme markdown-mode+ magit lsp-ui lsp-python lsp-php logview language-detection kaolin-theme jsx-mode jinja2-mode jedi inform info-colors highlight-unique-symbol highlight-thing highlight-tail highlight-symbol highlight-stages highlight-sexp highlight-quoted highlight-parentheses highlight-numbers highlight-defined highlight-cl highlight-chars hideshowvis helpful helm-slime haskell-mode handlebars-mode gruvbox-theme ggtags flycheck-package exec-path-from-shell esup emr elpy el-get dockerfile-mode docker-api docker dash-functional cyberpunk-theme counsel company-shell company-quickhelp company-lsp common-lisp-snippets color-theme-solarized color-theme-sanityinc-tomorrow color-theme-sanityinc-solarized color-theme-buffer-local color-theme-approximate col-highlight coffee-mode buffer-buttons babel-repl babel auto-overlays async-await anti-zenburn-theme add-node-modules-path ac-php 0xc))
 '(pos-tip-background-color "#DFE1DC")
 '(pos-tip-foreground-color "#4b5254")
 '(safe-local-variable-values
   '((Package . CL-WHO)
     (Package . FIVEAM)
     (Package . CCL)
     (package . puri)
     (Package ITERATE :use "COMMON-LISP" :colon-mode :external)
     (whitespace-style quote
		       (face trailing empty tabs))
     (whitespace-action)
     (Package . ODD-STREAMS)
     (Package . DRAKMA)
     (Encoding . utf-8)
     (readtable . runes)
     (Package . CXML)
     (Syntax . Common-Lisp)
     (Package . USOCKET)
     (Package . POSTMODERN)
     (Syntax . Ansi-Common-Lisp)
     (Package . CHUNGA)
     (Package . GROUPBY)
     (Package . TRIVIAL-GRAY-STREAMS)
     (indent-tabs)
     (Package . FLEXI-STREAMS)
     (Package . CL-PPCRE)
     (Package RT :use "COMMON-LISP" :colon-mode :external)
     (syntax . COMMON-LISP)
     (Package . CL-USER)
     (Package . HUNCHENTOOT)
     (Package . ANAPHORA)
     (Syntax . ANSI-Common-Lisp)
     (Package . CL-FAD)
     (Syntax . COMMON-LISP)
     (Package . BORDEAUX-THREADS)
     (Base . 10)
     (Syntax . ANSI-Common-lisp)))
 '(show-paren-mode t))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:background nil))))
 '(centaur-tabs-selected ((t (:background "light blue" :foreground "white"))))
 '(centaur-tabs-unselected ((t (:background "gainsboro" :foreground "grey50")))))

;; Prettify symbols
;; (defun push-prettify-symbols ()
;;   (setq prettify-symbols-alist
;;         (nconc prettify-symbols-alist
;;                (list (cons "<<" ?≪)
;;                      (cons ">>" ?≫)
;;                      (cons "->" ?→)
;;                      (cons "=>" ?⇒)
;;                      (cons "<=" ?≤)
;;                      (cons ">=" ?≥)))))

;; (add-hook 'lisp-mode-hook 'prettify-symbols-mode)
;; (add-hook 'fundamental-mode-hook 'prettify-symbols-mode)
;; (add-hook 'special-mode-hook 'prettify-symbols-mode)
;; (add-hook 'fundamental-mode-hook 'push-prettify-symbols)
;; (add-hook 'special-mode-hook 'push-prettify-symbols)
;; (add-hook 'php-mode-hook 'prettify-symbols-mode)
;; (add-hook 'php-mode-hook 'push-prettify-symbols)

;; (add-hook 'gml-mode-hook 'prettify-symbols-mode)
;; (add-hook 'gml-mode-hook 'push-prettify-symbols)

(add-hook 'emacs-lisp-mode-hook
          (lambda ()
            (setq company-backends '(company-elisp))
            (local-set-key (kbd "C-c RET") 'pp-macroexpand-last-sexp)))

(global-set-key (kbd "<C-tab>") 'company-complete)

(show-paren-mode t)
(highlight-symbol-mode t)

(add-hook 'after-init-hook 'global-company-mode)

(require 'flycheck)

;; turn on flychecking globally
(add-hook 'after-init-hook #'global-flycheck-mode)

(defun what-face (pos)
  (interactive "d")
  (let ((face (or (get-char-property (point) 'read-face-name)
                  (get-char-property (point) 'face))))
    (if face (message "Face: %s" face) (message "No face at %d" pos))))

(defalias 'elisp-repl 'ielm)
(defalias 'emacs-repl 'ielm)
(defalias 'repl 'ielm)

(defun apropos-package--filter (string)
  (let (packages)
    (dolist (package-assoc package-archive-contents)
      (let ((package (cadr package-assoc)))
        (when (or (string-match-p (regexp-quote string) (package-desc-summary package))
                  (string-match-p (regexp-quote string) (prin1-to-string (package-desc-name package))))
          (push package packages))))
    packages))

(defun apropos-package (string)
  (interactive "sSearch for package: ")
  ;; Initialize the package system if necessary.
  (unless package--initialized
    (package-initialize t))
  (let ((packages (apropos-package--filter string)))
    (if (null packages)
        (message "No packages")
      (package-show-package-list (mapcar 'package-desc-name packages)))))

(defalias 'apropos-info 'info-apropos)

(defun find-notes ()
  "Find TODO and FIXME notes in source code."
  (interactive)
  (grep-find "find . -type f -exec grep --color -nH --null -e  'TODO\\|FIXME' \{\} +"))

(defalias 'apropos-notes 'find-notes)

(defun what-overlays ()
  "List overlays at current cursor position."
  (interactive)
  (message (prin1-to-string (overlays-at (point)))))

(require 'dashboard)
(dashboard-setup-startup-hook)

(setq makeinfo-options "--no-validate --fill-column=70")

(require 'quicksearch)

;; (defun slime-break-on-entry ()
;;   (interactive)
;;   ;;(slime-compile-defun)
;;   (let* ((region (slime-region-for-defun-at-point))
;;       (source (buffer-substring-no-properties (first region) (second region))))
;;     (read-from-string source))

;;    ;;(slime-compile-string
;;    ;; (buffer-substring-no-properties start end)
;;    ;; start)
;;    )

(defun texinfo-wrap-in-@lisp ()
  (interactive)
  (goto-char (region-end))
  (insert "@end lisp")
  (goto-char (region-beginning))
  (insert "@lisp")
  (newline)
  (goto-char (region-end)))

(require 'repeat)

(require 'nml-mode)

(load "/home/marian/src/lisp/ten/ten.el")

(add-hook 'web-mode-hook 'ten-mode)

(require 'popwin)
(popwin-mode 1)

(defun eval-last-sexp-and-insert ()
  (interactive)
  (let ((result (eval-last-sexp)))
    (with-current-buffer (current-buffer)
      (insert (prin1-to-string result)))))

;; (use-package centaur-tabs
;;   :demand
;;   :config
;;   (centaur-tabs-mode t)
;;   :bind
;;   ("C-<prior>" . centaur-tabs-backward)
;;   ("C-<next>" . centaur-tabs-forward))

;; Replace default elisp-eldoc documentation function to display docstrings for functions too.
(defun elisp-eldoc-documentation-function ()
  "`eldoc-documentation-function' (which see) for Emacs Lisp."
  (let ((current-symbol (elisp--current-symbol))
        (current-fnsym  (elisp--fnsym-in-current-sexp)))
    (cond ((null current-fnsym)
           nil)
          ((eq current-symbol (car current-fnsym))
           (or (concat (apply #'elisp-get-fnsym-args-string current-fnsym)
                       (or (and (or (functionp (car current-fnsym))
                                    (macrop (car current-fnsym)))
                                (concat ". "
                                        (elisp--docstring-first-line (documentation (car current-fnsym)))))
                           ""))
               (elisp-get-var-docstring current-symbol)))
          (t
           (or (elisp-get-var-docstring current-symbol)
               (concat (apply #'elisp-get-fnsym-args-string current-fnsym)
                       (or (and (or (functionp (car current-fnsym))
                                    (macrop (car current-fnsym)))
                                (concat ". "
                                        (elisp--docstring-first-line (documentation (car current-fnsym)))))
                           "")))))))

(require 'json-navigator)

;; (add-to-list 'load-path "~/.emacs.d/el-get/el-get")

;; (unless (require 'el-get nil t)
;;   (url-retrieve
;;    "https://github.com/dimitri/el-get/raw/master/el-get-install.el"
;;    (lambda (s)
;;      (end-of-buffer)
;;      (eval-print-last-sexp))))

;; (push '(:name emacs-inspector
;;        :type git
;;        :url "git://github.com/mmontone/emacs-inspector.git"
;;        :features inspector
;;        :compile "inspector.el")
;;       el-get-sources)

;; (setq my:el-get-packages '())

;; (el-get 'sync my:el-get-packages)

(load-file "/home/marian/src/emacs-inspector/inspector.el")

(global-set-key (kbd "C-c i") 'inspect-expression)
(global-set-key (kbd "C-x i") 'inspect-last-sexp)

(require 'data-debug)

(defun chunyang-elisp-function-or-variable-quickhelp (symbol)
  "Display summary of function or variable at point.

Adapted from `describe-function-or-variable'."
  (interactive
   (let* ((v-or-f (variable-at-point))
          (found (symbolp v-or-f))
          (v-or-f (if found v-or-f (function-called-at-point))))
     (list v-or-f)))
  (if (not (and symbol (symbolp symbol)))
      (message "You didn't specify a function or variable")
    (let* ((fdoc (when (fboundp symbol)
                   (or (documentation symbol t) "Not documented.")))
           (fdoc-short (and (stringp fdoc)
                            (substring fdoc 0 (string-match "\n" fdoc))))
           (vdoc (when  (boundp symbol)
                   (or (documentation-property symbol 'variable-documentation t)
                       "Not documented as a variable.")))
           (vdoc-short (and (stringp vdoc)
                            (substring vdoc 0 (string-match "\n" vdoc)))))
      (and (require 'popup nil 'no-error)
           (popup-tip
            (or fdoc vdoc)
            :margin t)))))

(require 'pos-tip)

(defun pos-tip-describe-symbol (symbol)
  "Display documentation of SYMBOL in a tooltip."
  (interactive (list (symbol-at-point)))
  (when (and symbol (symbolp symbol))
    (let ((doc (or (and (functionp symbol) (documentation symbol))
		   (documentation-property symbol 'variable-documentation))))
      (when doc (pos-tip-show doc nil nil nil 0 nil nil 100 0)))))

(defun pos-tip-describe-cl-symbol (symbol)
  (interactive (list (symbol-at-point)))

  (when (and symbol (symbolp symbol))
    (let ((doc (slime-eval
		`(cl:with-output-to-string
		  (s)
		  (cl:let ((cl-symbol ,(slime-qualify-cl-symbol-name symbol)))
			  (cl:when (cl:stringp cl-symbol)
				   (cl:describe (cl:read-from-string cl-symbol) s)))))))
      (when (and doc (not (zerop (length doc))))
	(pos-tip-show doc nil nil nil 0 nil nil 100 0)))))

(defun pos-tip-describe-symbol-timer ()
  (when (eql major-mode 'emacs-lisp-mode)
    (pos-tip-describe-symbol (symbol-at-point)))
  (when (and (member major-mode '(slime-repl-mode lisp-mode))
	     (slime-connected-p))
    (pos-tip-describe-cl-symbol (symbol-at-point))))

;; (run-with-idle-timer
;;   2 t
;;  'pos-tip-describe-symbol-timer)

;; (defvar bootstrap-version)
;; (let ((bootstrap-file
;;        (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
;;       (bootstrap-version 5))
;;   (unless (file-exists-p bootstrap-file)
;;     (with-current-buffer
;;         (url-retrieve-synchronously
;;          "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
;;          'silent 'inhibit-cookies)
;;       (goto-char (point-max))
;;       (eval-print-last-sexp)))
;;   (load bootstrap-file nil 'nomessage))

;; (use-package el-docstring-sap
;;   :straight (el-docstring-sap :type git :host github :repo "rileyrg/el-docstring-sap" )
;;   :custom  (el-docstring-sap--display-func 'el-docstring-sap--posframe)
;;   :hook
;;   (emacs-lisp-mode . el-docstring-sap-mode)
;;   :bind
;;   ("M-<f2>" . el-docstring-sap-display)
;;   ("M-<f1>" . el-docstring-sap-mode))

;; (add-hook 'emacs-lisp-mode-hook 'el-docstring-sap-mode)

(require 'which-key)

(which-key-mode)

(require 'doom-modeline)
(doom-modeline-mode 1)

(require 'ox-texinfo)
(require 'ob-lisp)
(require 'restclient)

(require 'dired-icon)

(add-hook 'dired-mode-hook 'dired-icon-mode)

(defun dired-xdg-open ()
  "Open file at dired point using xdg-open command."
  (interactive)
  (start-process-shell-command "xdg-open" nil
			       (concatenate 'string "xdg-open \""
					    (dired-get-filename)
					    "\"")))

(defun fix-elisp-cl-deprecations ()
  (interactive)
  (replace-string "(defun*" "(cl-defun")
  (replace-string "(return-from" "(cl-return-from")
  (replace-string "(subseq" "(cl-subseq")) 
